package com.recommendation.api.repositories;

import com.recommendation.api.domain.Video;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VideoRepository extends MongoRepository<Video, String> {

    List<Video> findVideoByTitleRegex(String title);
}
