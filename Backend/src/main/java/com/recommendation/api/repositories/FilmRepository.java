package com.recommendation.api.repositories;

import com.recommendation.api.domain.Video;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FilmRepository<T extends Video> extends MongoRepository<T, String> {
}
