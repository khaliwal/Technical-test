package com.recommendation.api.repositories;

import com.recommendation.api.domain.VideoAud;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoAudRepository extends MongoRepository<VideoAud, String> {
}
