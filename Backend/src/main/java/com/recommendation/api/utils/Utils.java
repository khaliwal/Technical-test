package com.recommendation.api.utils;

import com.recommendation.api.domain.Film;
import com.recommendation.api.domain.Serie;
import com.recommendation.api.domain.Video;

import java.util.Objects;

public class Utils {

    public static boolean isNotCompleteFilm(Film film) {

        return Objects.isNull(film.getDirector())
                || film.getDirector().isBlank()
                || Objects.isNull(film.getReleaseDate());
    }

    public static boolean isNotCompleteSerie(Serie serie) {

        return serie.getNumberOfEpisodes() <= 0;
    }

    public static boolean isNotCompleteVideo(Video video) {

        return Objects.isNull(video)
                || Objects.isNull(video.getLabels())
                || video.getLabels().isEmpty()
                || Objects.isNull(video.getTitle())
                || video.getTitle().isBlank();
    }
}
