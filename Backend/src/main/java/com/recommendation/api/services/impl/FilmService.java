package com.recommendation.api.services.impl;

import com.recommendation.api.domain.Film;
import com.recommendation.api.repositories.FilmRepository;
import com.recommendation.api.services.IMultimediaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class FilmService implements IMultimediaService<Film> {

    private final Logger logger = Logger.getLogger("FilmService");

    private final FilmRepository<Film> filmRepository;

    public FilmService(FilmRepository<Film> filmRepository) {
        this.filmRepository = filmRepository;
    }

    @Override
    public Film addMultimedia(Film film) {
        logger.info(String.format("Demande de suppression du film %s", film.getTitle()));
        return filmRepository.save(film);
    }

    @Override
    public void deleteMultimedia(String id) {
        logger.info(String.format("Demande de suppression du film %s", id));
        filmRepository.deleteById(id);
    }

    @Override
    public List<Film> getMultimedia() {
        logger.info("Récupération des films");
        return filmRepository.findAll();
    }
}
