package com.recommendation.api.services;

import com.recommendation.api.domain.Video;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface IMultimediaService<T extends Video> {

     T addMultimedia(T t);
     void deleteMultimedia(String id);
     List<T> getMultimedia();


}
