package com.recommendation.api.services.impl;

import com.recommendation.api.domain.Video;
import com.recommendation.api.repositories.VideoRepository;
import com.recommendation.api.domain.VideoAud;
import com.recommendation.api.repositories.VideoAudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Service
public class VideoService {

    @Autowired
    private VideoRepository videoRepository;

    @Autowired
    private VideoAudRepository videoAudRepository;

    @Value("${score.min.recommendation}")
    private double minScoreRecommendation;

    public Function<Video, Video> addVideo = video -> videoRepository.save(video);

    public Supplier<List<Video>> getVideos = () -> videoRepository.findAll();

    public Function<String, Video> getVideoById = id -> videoRepository.findById(id).orElse(new Video());

    public Function<String, List<Video>> getVideoByTitle = title -> videoRepository.findVideoByTitleRegex(title);

    Function<Video, VideoAud> saveDeletedVideo = video -> videoAudRepository.save(new VideoAud(null, video.getId()));

    Consumer<String> deleteVideo = id -> videoRepository.deleteById(id);

    public Supplier<List<String>> getVideosDeleted = () -> videoAudRepository.findAll()
            .stream()
            .map(VideoAud::getIdVideoDeleted)
            .collect(Collectors.toList());


    /**
     * Méthode qui sauvegarde dans la table d'audit les id des vidéos supprimées
     * @param id
     * @return Une chaîne de caractères indiquant le succès ou non de la suppression.
     */
    public String deleteVideo(String id) {

        Optional<Video> video = videoRepository.findById(id);
        if (video.isPresent()) {
            saveDeletedVideo.apply(video.get());
            deleteVideo.accept(video.get().getId());
            return String.format("Vidéo %s supprimée",video.get().getId());
        }
        return "Vidéo non trouvée";
    }

    /**
     * Méthode qui est appelée par le contrôleur pour l'endpoint de vidéos similaires.
     * Actuellement le seuil requis est de 0.5 (modifiable dans application.properties)
     * @param id
     * @return Une liste de recommandation de vidéos en fonctions
     * des labels associés à la vidéo passée en paramètre.
     */
    public List<Video> getSimilarVideo(String id) {

        Optional<Video> video = videoRepository.findById(id);
        if (video.isEmpty()) {
            return Collections.emptyList();
        }

        return videoRepository.findAll().stream()
                .filter(video1 -> !video1.getId().equals(video.get().getId()) &&
                        getJaccardDistance(video.get().getLabels(),
                                video1.getLabels()) >= this.minScoreRecommendation)
                .collect(Collectors.toList());
    }

    /**
     *
     * @param labels1
     * @param labels2
     * @return La distance de Jaccars qui permet d'estimer la correspondance entre deux vidéos via leurs labels.
     * @throws ArithmeticException
     */
    private double getJaccardDistance(List<String> labels1, List<String> labels2) throws ArithmeticException{

        try {
            return getIntersection.apply(labels1, labels2) / getUnion.apply(labels1, labels2);
        } catch (ArithmeticException e) {
            throw new ArithmeticException("Division par 0, problème sur les labels");
        }
    }

    /**
     *  BiFunction qui en fonction de deux listes de labels calcule l'intersection
     *  pour appliquer la fonction de calcul de la distance de Jaccard
     */
    private final BiFunction<List<String>, List<String>, Double> getIntersection =
            (l1, l2) -> (double) l1.stream()
                    .filter(l2::contains)
                    .count();

    /**
     *  BiFunction qui en fonction de deux listes de labels calcule l'union
     *  pour appliquer la fonction de calcul de la distance de Jaccard
     */
    private final BiFunction<List<String>, List<String>, Double> getUnion =
            (l1, l2) -> l1.size() + l2.size() - getIntersection.apply(l1, l2);
}

