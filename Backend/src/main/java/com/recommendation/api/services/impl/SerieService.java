package com.recommendation.api.services.impl;

import com.recommendation.api.repositories.SerieRepository;
import com.recommendation.api.domain.Serie;
import com.recommendation.api.services.IMultimediaService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.logging.Logger;

@Service
public class SerieService implements IMultimediaService<Serie> {

    private final Logger logger = Logger.getLogger("FilmService");

    private final SerieRepository<Serie> serieRepository;

    public SerieService(SerieRepository<Serie> serieRepository) {
        this.serieRepository = serieRepository;
    }

    @Override
    public Serie addMultimedia(Serie serie) {
        logger.info(String.format("Demande d'ajout de la série %s", serie.getTitle()));
        return serieRepository.save(serie);
    }

    @Override
    public void deleteMultimedia(String id) {
        logger.info(String.format("Demande de suppression de la série %s", id));
        serieRepository.deleteById(id);
    }

    @Override
    public List<Serie> getMultimedia() {
        logger.info("Récupération des séries");
        return serieRepository.findAll();
    }
}
