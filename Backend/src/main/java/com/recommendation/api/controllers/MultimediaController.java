package com.recommendation.api.controllers;

import com.recommendation.api.domain.Film;
import com.recommendation.api.domain.Video;
import com.recommendation.api.domain.Serie;
import com.recommendation.api.services.impl.FilmService;
import com.recommendation.api.services.impl.SerieService;
import com.recommendation.api.services.impl.VideoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import static com.recommendation.api.utils.Utils.*;

@RestController
@RequestMapping("/api/v1/multimedia")
public class MultimediaController {

    private final Logger logger = Logger.getLogger("MultimediaController");

    private final FilmService filmService;

    private final VideoService videoService;

    private final SerieService serieService;

    public MultimediaController(FilmService filmService, VideoService videoService, SerieService serieService) {
        this.filmService = filmService;
        this.videoService = videoService;
        this.serieService = serieService;
    }

    @PostMapping("/film")
    public ResponseEntity<Film> addFilm(@RequestBody Film film) {

        logger.info("Demande d'ajout d'un film");
        if (isNotCompleteVideo(film) || isNotCompleteFilm(film)) {
            logger.warning("Film incomplet");
            return new ResponseEntity<>(new Film(), HttpStatus.BAD_REQUEST);
        }
        logger.info(String.format("Ajout avec succès du film : %s",film.getTitle()));
        return ResponseEntity.ok().body(filmService.addMultimedia(film));
    }

    @GetMapping("/film")
    public ResponseEntity<List<Film>> getFilms() {

        return ResponseEntity.ok().body(filmService.getMultimedia());
    }

    @PostMapping("/serie")
    public ResponseEntity<Serie> addSerie(@RequestBody Serie serie) {

        logger.info("Demande d'ajout d'une série");
        if (isNotCompleteVideo(serie) || isNotCompleteSerie(serie)) {
            logger.warning("Série incomplète");
            return new ResponseEntity<>(new Serie(), HttpStatus.BAD_REQUEST);
        }
        logger.info(String.format("Ajout avec succès de la série : %s", serie.getTitle()));
        return ResponseEntity.ok().body(serieService.addMultimedia(serie));
    }

    @DeleteMapping("/serie/{id}")
    public ResponseEntity<String> deleteSerie(@PathVariable String id) {

        logger.info("Demande de suppression d'une série");
        if (Objects.isNull(id) || id.isBlank()) {
            logger.warning("id invalide");
            return new ResponseEntity<String>("id invalide", HttpStatus.BAD_REQUEST);
        }
        logger.info(String.format("Suppression réussie de la série : %s", id));
        serieService.deleteMultimedia(id);
        return ResponseEntity.ok().body("Suppression réussie de la série");
    }

    @GetMapping("/serie")
    public ResponseEntity<List<Film>> getSeries() {

        return ResponseEntity.ok().body(filmService.getMultimedia());
    }

    @PostMapping("/video")
    public ResponseEntity<Video> addVideo(@RequestBody Video video) {

        logger.info("Demande d'ajout d'une vidéo");
        if (isNotCompleteVideo(video)) {
            logger.warning("Vidéo incomplète");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info(String.format("Ajout avec succès de la vidéo : %s",video.getTitle()));
        return ResponseEntity.ok().body(videoService.addVideo.apply(video));
    }

    @GetMapping("/video")
    public ResponseEntity<List<Video>> getVideos() {

        return ResponseEntity.ok().body(videoService.getVideos.get());
    }

    @GetMapping("/video/{id}")
    public ResponseEntity<Video> getVideoById(@PathVariable String id) {

        return ResponseEntity.ok().body(videoService.getVideoById.apply(id));
    }

    @GetMapping("/video/title/{title}")
    public ResponseEntity<List<Video>> getVideoByTitle(@PathVariable String title) {

        logger.info("Demande de récupération d'une vidéo par titre");
        if (Objects.isNull(title) || title.length() < 3 || title.isBlank()) {
            logger.info("La demande de récupération a échoué");
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        logger.info("Récupération complète d'une vidéo par titre");
        return ResponseEntity.ok().body(videoService.getVideoByTitle.apply(title));
    }

    @DeleteMapping("/video/{id}")
    public ResponseEntity<String> deleteVideoById(@PathVariable String id) {

        if (Objects.isNull(id) || id.isBlank()) {
            return new ResponseEntity<>("Merci de rentrer un id", HttpStatus.BAD_REQUEST);
        }
        logger.info("Suppression réussie de la vidéo");
        return ResponseEntity.ok().body(videoService.deleteVideo(id));
    }

    @GetMapping("/video/deleted")
    public ResponseEntity<List<String>> getVideosDeleted() {
        return ResponseEntity.ok().body(videoService.getVideosDeleted.get());
    }

    @GetMapping("/video/recommended/{id}")
    public ResponseEntity<List<Video>> getRecommendedVideos(@PathVariable String id) {
        logger.info("Récupération des titres recommandés");
        return ResponseEntity.ok(videoService.getSimilarVideo(id));
    }



}
