package com.recommendation.api.domain;

import java.util.Calendar;
import java.util.List;

public class Film extends Video {

    private String director;

    private Calendar releaseDate;

    public Film(String id, String title, List<String> labels, String director, Calendar releaseDate) {
        super(id, title, labels);
        this.director = director;
        this.releaseDate = releaseDate;
    }

    public Film() {
        super(null, null, null);
        this.director = null;
        this.releaseDate = null;
    }

    public String getDirector() {
        return director;
    }

    public Calendar getReleaseDate() {
        return releaseDate;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public void setReleaseDate(Calendar releaseDate) {
        this.releaseDate = releaseDate;
    }
}
