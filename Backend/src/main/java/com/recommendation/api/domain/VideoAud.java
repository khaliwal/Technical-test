package com.recommendation.api.domain;

public class VideoAud {

    private String id;

    private String idVideoDeleted;

    public VideoAud(String id, String idVideoDeleted) {
        this.id = id;
        this.idVideoDeleted = idVideoDeleted;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVideoDeleted() {
        return idVideoDeleted;
    }

    public void setIdVideoDeleted(String idVideoDeleted) {
        this.idVideoDeleted = idVideoDeleted;
    }
}
