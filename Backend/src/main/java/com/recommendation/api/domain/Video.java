package com.recommendation.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class Video {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String id;

    private String title;

    private List<String> labels;

    public Video(String id, String title, List<String> labels) {
        this.id = id;
        this.title = title;
        this.labels = labels;
    }

    public Video() {
        this.id = null;
        this.title = null;
        this.labels = null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }
}
