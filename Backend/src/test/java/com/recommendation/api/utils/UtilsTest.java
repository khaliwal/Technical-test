package com.recommendation.api.utils;

import com.recommendation.api.domain.Film;
import com.recommendation.api.domain.Serie;
import com.recommendation.api.domain.Video;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;
import java.util.List;

import static com.recommendation.api.utils.Utils.*;

@RunWith(SpringRunner.class)
public class UtilsTest {

    @Test
    public void isNotCompleteFilm_should_return_true() {

        // GIVEN
        Film film = new Film("id", "title1", List.of("horreur", "jeux", "humour", "peur"), "director",
                null);
        Film film2 = new Film("id", "title1", List.of("horreur", "jeux", "humour", "peur"), null,
                Calendar.getInstance());
        Film film3 = new Film("id", "title1", List.of("horreur", "jeux", "humour", "peur"), "",
                Calendar.getInstance());
        // THEN
        Assert.assertTrue(isNotCompleteFilm(film));
        Assert.assertTrue(isNotCompleteFilm(film2));
        Assert.assertTrue(isNotCompleteFilm(film3));
    }

    @Test
    public void isNotCompleteFilm_should_return_false() {

        // GIVEN
        Film film = new Film("id", "title1", List.of("horreur", "jeux", "humour", "peur"), "director",
                Calendar.getInstance());

        // THEN
        Assert.assertFalse(isNotCompleteFilm(film));
    }

    @Test
    public void isNotCompleteVideo_should_return_true() {

        // GIVEN
        Video video = new Video("id", null, List.of("horreur", "jeux", "humour", "peur"));
        Video video2 = new Video("id", "", List.of("horreur", "jeux", "humour", "peur"));
        Video video3 = new Video("id", "title1", null);

        // THEN
        Assert.assertTrue(isNotCompleteVideo(video));
        Assert.assertTrue(isNotCompleteVideo(video2));
        Assert.assertTrue(isNotCompleteVideo(video3));
    }

    @Test
    public void isNotCompleteVideo_should_return_false() {

        // GIVEN
        Video video = new Video("id", "title", List.of("horreur", "jeux", "humour", "peur"));

        // THEN
        Assert.assertFalse(isNotCompleteVideo(video));
    }


    @Test
    public void isNotCompleteSerie_should_return_true() {

        // GIVEN
        Serie serie = new Serie("id", "title1", List.of("peur"), 0);

        // THEN
        Assert.assertTrue(isNotCompleteSerie(serie));
    }

    @Test
    public void isNotCompleteSerie_should_return_false() {

        // GIVEN
        Serie serie = new Serie("id", "title1", List.of("peur"), 10);

        // THEN
        Assert.assertFalse(isNotCompleteSerie(serie));
    }
}
