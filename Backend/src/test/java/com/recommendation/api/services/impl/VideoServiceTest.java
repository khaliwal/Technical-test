package com.recommendation.api.services.impl;

import com.recommendation.api.domain.Video;
import com.recommendation.api.domain.VideoAud;
import com.recommendation.api.repositories.VideoAudRepository;
import com.recommendation.api.repositories.VideoRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class VideoServiceTest {

    @Mock
    private VideoRepository videoRepository;

    @Mock
    private VideoAudRepository videoAudRepository;

    @InjectMocks
    private VideoService videoService;


    @Before
    public void initializeData() {
        ReflectionTestUtils.setField(videoService, "minScoreRecommendation", 0.5);
    }

    @Test
    public void deleteVideo_should_return_success_message() {

        // GIVEN
        Optional<Video> optionalVideo = Optional.of(new Video("id", "title", Collections.emptyList()));

        // WHEN
        Mockito.when(videoRepository.findById("id")).thenReturn(optionalVideo);
        Mockito.when(videoAudRepository.save(new VideoAud(null,"id")))
                .thenReturn(new VideoAud(null, "id"));
        // THEN
        Assert.assertEquals("Vidéo id supprimée", videoService.deleteVideo("id"));
    }

    @Test
    public void deleteVideo_should_return_failed_message() {

        // GIVEN
        Optional<Video> optionalVideo = Optional.ofNullable(null);

        // WHEN
        Mockito.when(videoRepository.findById("id")).thenReturn(optionalVideo);
        Mockito.when(videoAudRepository.save(new VideoAud(null,"id")))
                .thenReturn(new VideoAud(null, "id"));
        // THEN
        Assert.assertEquals("Vidéo non trouvée", videoService.deleteVideo("id"));
    }

    @Test
    public void getSimilarVideo_should_return_list_of_video() {

        // GIVEN
        Video v1 = new Video("id", "title1", List.of("horreur", "jeux", "humour", "peur"));
        Video v2 = new Video("id2", "title2", List.of("enfant", "jouet", "unboxing"));
        Video v3 = new Video("id3", "title3", List.of("horreur", "jeux", "humour", "comédie"));

        // WHEN
        Mockito.when(videoRepository.findById("id")).thenReturn(Optional.of(v1));
        Mockito.when(videoRepository.findAll()).thenReturn(List.of(v1, v2, v3));

        // THEN
        Assert.assertEquals(List.of(v3), videoService.getSimilarVideo("id"));
    }

    @Test
    public void getSimilarVideo_should_return_empty_list() {

        // GIVEN
        Video v1 = new Video("id", "title1", List.of("horreur", "jeux", "humour", "peur"));
        Video v2 = new Video("id2", "title2", List.of("enfant", "jouet", "unboxing"));
        Video v3 = new Video("id3", "title3", List.of("horreur", "jeux", "humour", "comédie"));

        // WHEN
        Mockito.when(videoRepository.findAll()).thenReturn(List.of(v1, v2, v3));

        // THEN
        Assert.assertEquals(Collections.emptyList(), videoService.getSimilarVideo("id"));
    }

    @Test
    public void getVideosDeleted_should_return_list_of_ids() {

        // GIVEN
        VideoAud v1 = new VideoAud("id", "idDeleted1");
        VideoAud v2 = new VideoAud("id2", "idDeleted2");
        VideoAud v3 = new VideoAud("id3", "idDeleted3");

        // WHEN
        Mockito.when(videoAudRepository.findAll()).thenReturn(List.of(v1, v2, v3));

        // THEN
        Assert.assertEquals(List.of("idDeleted1", "idDeleted2", "idDeleted3"),
                videoService.getVideosDeleted.get());
    }


}
