# Application Spring Boot 11 pour Spideo :

(Préalablement avoir maven d'installé)

Avant de lancer le projet vérifier que le port 27018 est disponible, dans le cas contraire modifier le port
dans le fichier application.properties ligne "spring.data.mongodb.port".

Pour lancer le projet, se rendre dans le dossier "Backend" et exécuter "mvn spring-boot:run" ou ouvrir le dossier "Backend" via un IDE 
et lancer le run du projet.

Différents endpoints sont implémentés, pour avoir un aperçu de comment les utiliser, se rendre à l'url 
suivante "http://localhost:8080/swagger-ui/" où se trouve une documentation avec les différents json à utiliser.
